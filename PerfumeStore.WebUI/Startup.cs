﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PerfumeStore.WebUI.Startup))]
namespace PerfumeStore.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}