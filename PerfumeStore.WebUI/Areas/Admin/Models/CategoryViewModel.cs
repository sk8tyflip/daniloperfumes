﻿using PerfumeStore.Domain.Identity;
using PerfumeStore.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PerfumeStore.WebUI.Areas.Admin.Models
{
    public class CategoryViewModel
    {
        public int CategoryId { get; set; }
        [Required]
        public String CategoryName { get; set; }
    }
}