﻿using System.Web.Mvc;

namespace PerfumeStore.WebUI.Controllers
{
    //[OutputCache(CacheProfile = "StaticUser")]
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            
            return View();
        }
    }
}