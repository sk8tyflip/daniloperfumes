﻿using PerfumeStore.Domain.Helper;
using PerfumeStore.Domain.Identity;
using PerfumeStore.Domain.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfumeStore.Domain.Infrastructure
{
    public class IdentityDbInitializer : DropCreateDatabaseIfModelChanges<PerfumeStoreDBContext>
    {
        protected override void Seed(PerfumeStoreDBContext context)
        {
            PerformInitialSetup(context);
            base.Seed(context);
        }

        public void PerformInitialSetup(PerfumeStoreDBContext context)
        {
            GetRoles().ForEach(c => context.Roles.Add(c));
            GetCategories().ForEach(c => context.Categories.Add(c));
            GetProducts().ForEach(c => context.Products.Add(c));
            context.SaveChanges();
            Helper.PerfumeStore hasher = new Helper.PerfumeStore();
            var user = new AppUser { UserName = "admin", Email = "admin@perfumestore.com", PasswordHash = hasher.HashPassword("admin"), Membership = "Admin" };
            var role = context.Roles.Where(r => r.Name == "Admin").First();
            user.Roles.Add(new IdentityUserRole { RoleId = role.Id, UserId = user.Id });
            context.Users.Add(user);
            user = new AppUser { UserName = "regular", Email = "regular@perfumestore.com", PasswordHash = hasher.HashPassword("regular"), Membership = "Regular" };
            role = context.Roles.Where(r => r.Name == "Regular").First();
            user.Roles.Add(new IdentityUserRole { RoleId = role.Id, UserId = user.Id });
            context.Users.Add(user);
            user = new AppUser { UserName = "advanced", Email = "advanced@perfumestore.com", PasswordHash = hasher.HashPassword("advanced"), Membership = "Advanced" };
            role = context.Roles.Where(r => r.Name == "Advanced").First();
            user.Roles.Add(new IdentityUserRole { RoleId = role.Id, UserId = user.Id });
            context.Users.Add(user);
            context.SaveChanges();
        }

        private static List<AppRole> GetRoles()
        {
            var roles = new List<AppRole> {
               new AppRole {Name="Admin", Description="Admin"},
               new AppRole {Name="Regular", Description="Regular"},
               new AppRole {Name="Advanced", Description="Advanced"}
            };

            return roles;
        }

        private static List<Category> GetCategories()
        {
            var categories = new List<Category> {
               new Category {CategoryId=1, CategoryName="Male"},
               new Category {CategoryId=2, CategoryName="Female"},
               new Category {CategoryId=3, CategoryName="Unisex"}
            };

            return categories;
        }

        private static List<Product> GetProducts()
        {
            var products = new List<Product>();
            products.AddRange
                (
                new[]
                {
                    new Product { ProductId=1, ProductName="Versace EROS", CategoryId=1, Price=399.00, Image="male/male1.jpg", Discount=10, Condition="New", UserId="Admin" },
                    new Product { ProductId=2, ProductName="Versace EROS Flame", CategoryId=1, Price=345, Image="male/male2.jpg", Discount=10, Condition="New", UserId="Admin" },
                    new Product { ProductId=3, ProductName="Chanel", CategoryId=2, Price=875, Image="female/female1.jpg", Discount=10, Condition="New", UserId="Admin" },
                    new Product { ProductId=4, ProductName="Coco Chanel", CategoryId=2, Price=563, Image="female/female2.jpg", Discount=10, Condition="New", UserId="Admin" },
                    new Product { ProductId=5, ProductName="Molecule 02", CategoryId=3, Price=234, Image="unisex/uni1.jpg", Discount=10, Condition="New", UserId="Admin" },
                    new Product { ProductId=6, ProductName="Davidoff Cool Water", CategoryId=3, Price=562, Image="unisex/uni2.jpg", Discount=10, Condition="New", UserId="Admin" },

                }
                );

            return products;
        }
    }
}
