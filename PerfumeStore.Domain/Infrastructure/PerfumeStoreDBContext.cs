﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerfumeStore.Domain.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using PerfumeStore.Domain.Identity;

namespace PerfumeStore.Domain.Infrastructure
{
    public partial class PerfumeStoreDBContext : IdentityDbContext<AppUser>
    {
        public PerfumeStoreDBContext()
            : base("name=PerfumeStoreDBContext", throwIfV1Schema: false)
        {
            Database.SetInitializer<PerfumeStoreDBContext>(new IdentityDbInitializer());
        }

        public static PerfumeStoreDBContext Create()
        {
            return new PerfumeStoreDBContext();
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
    }
}
