﻿using PerfumeStore.Domain.Infrastructure;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfumeStore.Domain.Identity
{
    public class AppUserStore : UserStore<AppUser>
    {
        public AppUserStore(PerfumeStoreDBContext context) : base(context)
        {

        }
    }
}
